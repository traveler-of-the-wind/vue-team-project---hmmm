# hmmm-complete

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### remove git hook
```json
,
  "gitHooks": {
    "pre-commit": "lint-staged"
  },
  "lint-staged": {
    "*.{js,jsx,vue}": [
      "vue-cli-service lint",
      "git add"
    ]
  }
```

###  成员任务分支

王增: wz-selectQuestion
	  wz-Subjects
###  成员任务分支
  徐闰: xr-list 目录
  徐闰: xr-manage 面试技巧
