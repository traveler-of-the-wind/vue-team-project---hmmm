const path = require('path')
let externals = {}
if (process.env.NODE_ENV === 'production') {
  externals = {
    // import导包的包名: window全局的成员名称
    // echarts: 'echarts',
    vue: 'Vue',
    'element-ui': 'ELEMENT',
    'vue-quill-editor': 'VueQuillEditor'
  }
}
module.exports = {
  publicPath: './',
  lintOnSave: false,
  chainWebpack: config => {
    config.plugin('html').tap(args => {
      args[0].title = '二组团队项目-黑马面面'
      return args
    })
    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule
      .test(/.svg$/)
      .include.add(path.resolve(__dirname, './src/icons/svg'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
    const fileRule = config.module.rule('file')
    fileRule.uses.clear()
    fileRule
      .test(/.svg$/)
      .exclude.add(path.resolve(__dirname, './src/icons/svg'))
      .end()
      .use('file-loader')
      .loader('file-loader')
  },
  configureWebpack: {
    // 打包优化(上线时是否排除这些包)
    externals: externals
  }
}
