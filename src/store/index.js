import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import route from './route'
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    route
  },
  getters
})

export default store
