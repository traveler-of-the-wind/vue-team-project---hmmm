const state = {
  objPage: {
    page: 1, // 页码值
    pagesize: 10 // 每页显示的条数
  },
  tagName: '',
  newID: '',
  requestParameters: {
    page: 1,
    pagesize: 10
  },
  listID: ''
}

const mutations = {
  listID (state, data) {
    state.listID = data
  },
  setObj (state, { subjectID, tagName }) {
    state.tagName = tagName
    state.objPage = {
      page: 1, // 页码值
      pagesize: 10, // 每页显示的条数
      subjectID
    }
  },
  newObj (state, data) {
    state.newID = data
  },
  directorysObj (state, { subjectID }) {
    state.requestParameters = {
      page: 1, // 页码值
      pagesize: 10, // 每页显示的条数
      subjectID
    }
  }
}

const actions = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
