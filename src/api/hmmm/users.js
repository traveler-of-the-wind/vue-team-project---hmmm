import { createAPI } from '@/utils/request'
// 标签列表
export const users = (data) => createAPI('/users/simple', 'get', data)
