import Vue from 'vue'
import 'normalize.css/normalize.css' // A modern alternative to CSS resets
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '@/styles/index.scss' // global css
import App from './App'
import router from './router'
import store from './store'
import i18n from './lang' // Internationalization
import './icons' // icon
import './errorLog' // error log
import * as filters from './filters' // global filters
import './mock' // simulation data
// font-awesome
import 'font-awesome/css/font-awesome.css'
// 富文本的样式
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
/*
 * 注册 - 业务模块
 */
import dashboard from '@/module-dashboard/' // 面板
import base from '@/module-manage/' // 用户管理
import hmmm from '@/module-hmmm/' // 黑马面面
import dayjs from 'dayjs'
// 导入富文本编辑器
import VueQuillEditor from 'vue-quill-editor'

Vue.use(dashboard, store)
Vue.use(base, store)
// Vue.use(list, store)
// Vue.use(form, store)
// Vue.use(details, store)
Vue.use(hmmm, store)

// 全局使用富文本
Vue.use(VueQuillEditor)

/*
 * 注册 - 组件
 */
// 时间过滤器

Vue.filter('formatTimes', obj => {
  return dayjs(obj).format('YYYY-MM-DD')
})
// 饿了么
Vue.use(Element, {
  size: 'medium', // set element-ui default size
  i18n: (key, value) => i18n.t(key, value)
})
// 过滤器
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})
// 格式化时间
Vue.filter('formdate', time => {
  var date = new Date(time)
  var y = date.getFullYear()
  var m = date.getMonth() + 1
  m = m < 10 ? '0' + m : m
  var d = date.getDate()
  d = d < 10 ? '0' + d : d
  var hh = date.getHours()
  hh = hh < 10 ? '0' + hh : hh
  var mm = date.getMinutes()
  mm = mm < 10 ? '0' + mm : mm
  var ss = date.getSeconds()
  ss = ss < 10 ? '0' + ss : ss
  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
})

// 格式化时间
Vue.filter('formdate', time => {
  var date = new Date(time)
  var y = date.getFullYear()
  var m = date.getMonth() + 1
  m = m < 10 ? '0' + m : m
  var d = date.getDate()
  d = d < 10 ? '0' + d : d
  var hh = date.getHours()
  hh = hh < 10 ? '0' + hh : hh
  var mm = date.getMinutes()
  mm = mm < 10 ? '0' + mm : mm
  var ss = date.getSeconds()
  ss = ss < 10 ? '0' + ss : ss
  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
})
Vue.config.productionTip = false

/* eslint-disable */
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(App)
})
